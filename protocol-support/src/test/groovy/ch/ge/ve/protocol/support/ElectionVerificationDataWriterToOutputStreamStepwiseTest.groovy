/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support

import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOTS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOT_KEYS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATIONS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATION_KEYS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DECRYPTION_PROOFS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DONE
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_OFFICER_KEY_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_SET_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.GENERATORS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.INITIALIZED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PARTIAL_DECRYPTIONS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PRIMES_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_CREDENTIALS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_KEY_PARTS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_PARAMETERS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLES_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLE_PROOFS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.TALLY_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.UNINITIALIZED
import static ch.ge.ve.protocol.support.WriterUtils.CHARSET

import ch.ge.ve.protocol.model.BallotAndQuery
import ch.ge.ve.protocol.model.Confirmation
import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.DecryptionProof
import ch.ge.ve.protocol.model.Decryptions
import ch.ge.ve.protocol.model.ElectionSetForVerification
import ch.ge.ve.protocol.model.Encryption
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.protocol.model.Point
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.model.ShuffleProof
import ch.ge.ve.protocol.model.Tally
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.io.CharStreams
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class ElectionVerificationDataWriterToOutputStreamStepwiseTest extends Specification {
  @Shared
  OutputStream outputStream
  @Shared
  ObjectMapper objectMapper

  // Values to be written
  @Shared
  PublicParameters publicParameters
  @Shared
  ElectionSetForVerification electionSet
  @Shared
  List<BigInteger> primes
  @Shared
  List<BigInteger> generators
  @Shared
  Map<Integer, EncryptionPublicKey> publicKeyParts
  @Shared
  EncryptionPublicKey electionOfficerPublicKey
  @Shared
  Map<Integer, Point> cc0PublicCredParts
  @Shared
  Map<Integer, Point> cc1PublicCredParts
  @Shared
  BallotAndQuery ballot0
  @Shared
  BallotAndQuery ballot1
  @Shared
  Confirmation confirmation0
  @Shared
  Confirmation confirmation1
  @Shared
  List<Encryption> shuffle0
  @Shared
  List<Encryption> shuffle1
  @Shared
  ShuffleProof shuffleProof0
  @Shared
  ShuffleProof shuffleProof1
  @Shared
  Decryptions partialDecryption0
  @Shared
  Decryptions partialDecryption1
  @Shared
  Decryptions partialDecryption2
  @Shared
  DecryptionProof decryptionProof0
  @Shared
  DecryptionProof decryptionProof1
  @Shared
  DecryptionProof decryptionProof2
  @Shared
  Tally tally

  @Shared
  String pubCreds0Json
  @Shared
  String pubCreds1Json
  @Shared
  String ballotsJson
  @Shared
  String confirmationsJson
  @Shared
  String shuffle0Json
  @Shared
  String shuffle1Json
  @Shared
  String shuffleProof0Json
  @Shared
  String shuffleProof1Json
  @Shared
  String partialDecryption0Json
  @Shared
  String partialDecryption1Json
  @Shared
  String partialDecryption2Json
  @Shared
  String decryptionProof0Json
  @Shared
  String decryptionProof1Json
  @Shared
  String decryptionProof2Json
  @Shared
  String tallyJson

  @Shared
  ElectionVerificationDataWriter electionVerificationDataWriter

  def setupSpec() {
    // collaborators
    outputStream = new ByteArrayOutputStream()
    objectMapper = Mock()

    // data to be written
    publicParameters = PublicParametersFactory.LEVEL_0.createPublicParameters(2)
    electionSet = Mock()
    electionSet.getVoterCount() >> 4
    // Spock has issues matching various arguments of the same type.
    primes = [BigInteger.ZERO]
    generators = [BigInteger.ONE]
    publicKeyParts = Mock()
    electionOfficerPublicKey = new EncryptionPublicKey(BigInteger.ZERO, publicParameters.encryptionGroup)
    cc0PublicCredParts = [
        0: new Point(BigInteger.ZERO, BigInteger.ZERO),
        1: new Point(BigInteger.ZERO, BigInteger.ONE),
        2: new Point(BigInteger.ONE, BigInteger.ZERO),
        3: new Point(BigInteger.ONE, BigInteger.ONE)
    ]
    cc1PublicCredParts = [
        0: new Point(BigInteger.ONE, BigInteger.ONE),
        1: new Point(BigInteger.ONE, BigInteger.ZERO),
        2: new Point(BigInteger.ZERO, BigInteger.ONE),
        3: new Point(BigInteger.ZERO, BigInteger.ZERO)
    ]
    ballot0 = new BallotAndQuery(BigInteger.ZERO, [], new NonInteractiveZkp([], []))
    ballot1 = new BallotAndQuery(BigInteger.ONE, [], new NonInteractiveZkp([], []))
    confirmation0 = new Confirmation(BigInteger.ZERO, new NonInteractiveZkp([], []))
    confirmation1 = new Confirmation(BigInteger.ONE, new NonInteractiveZkp([], []))
    shuffle0 = [new Encryption(BigInteger.ZERO, BigInteger.ZERO)]
    shuffle1 = [new Encryption(BigInteger.ONE, BigInteger.ONE)]
    shuffleProof0 = new ShuffleProof(
        new ShuffleProof.T(BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, [BigInteger.ZERO, BigInteger.ZERO], [BigInteger.ZERO]),
        new ShuffleProof.S(BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, [BigInteger.ZERO], [BigInteger.ZERO]),
        [],
        [])
    shuffleProof1 = new ShuffleProof(
        new ShuffleProof.T(BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, [BigInteger.ONE, BigInteger.ONE], [BigInteger.ONE]),
        new ShuffleProof.S(BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, [BigInteger.ONE], [BigInteger.ONE]),
        [],
        [])
    partialDecryption0 = new Decryptions([BigInteger.valueOf(0L)])
    partialDecryption1 = new Decryptions([BigInteger.valueOf(1L)])
    partialDecryption2 = new Decryptions([BigInteger.valueOf(2L)])
    decryptionProof0 = new DecryptionProof([], BigInteger.ZERO)
    decryptionProof1 = new DecryptionProof([], BigInteger.ONE)
    decryptionProof2 = new DecryptionProof([], BigInteger.valueOf(2L))
    tally = new Tally(Collections.singletonMap(new CountingCircle(0, "countingCircle", "countingCircleName"), []))

    // create the object under test
    electionVerificationDataWriter = new ElectionVerificationDataWriterToOutputStream(objectMapper, outputStream, true)

    // Object mapper interactions
    objectMapper.writeValueAsString(publicParameters) >> "PP_PLACEHOLDER"
    objectMapper.writeValueAsString(electionSet) >> "ES_PLACEHOLDER"
    objectMapper.writeValueAsString(primes) >> "PRIMES_PLACEHOLDER"
    objectMapper.writeValueAsString(generators) >> "GENERATORS_PLACEHOLDER"
    objectMapper.writeValueAsString(publicKeyParts) >> "PUB_KEY_MAP"
    objectMapper.writeValueAsString(electionOfficerPublicKey) >> "EO_KEY"
    objectMapper.writeValueAsString(_ as Point) >> { args ->
      def point = args[0] as Point
      return String.format("{%n\"x\": %d,%n\"y\": %d\n}", point.x.intValue(), point.y.intValue())
    }
    objectMapper.writeValueAsString(ballot0) >> "BALLOT_0"
    objectMapper.writeValueAsString(ballot1) >> "BALLOT_1"
    objectMapper.writeValueAsString(confirmation0) >> "CONFIRMATION_0"
    objectMapper.writeValueAsString(confirmation1) >> "CONFIRMATION_1"
    objectMapper.writeValueAsString(shuffle0) >> "SHUFFLE_0"
    objectMapper.writeValueAsString(shuffle1) >> "SHUFFLE_1"
    objectMapper.writeValueAsString(shuffleProof0) >> "SHUFFLE_PROOF_0"
    objectMapper.writeValueAsString(shuffleProof1) >> "SHUFFLE_PROOF_1"
    objectMapper.writeValueAsString(partialDecryption0) >> "PARTIAL_DECRYPTION_0"
    objectMapper.writeValueAsString(partialDecryption1) >> "PARTIAL_DECRYPTION_1"
    objectMapper.writeValueAsString(partialDecryption2) >> "PARTIAL_DECRYPTION_2"
    objectMapper.writeValueAsString(decryptionProof0) >> "DECRYPTION_PROOF_0"
    objectMapper.writeValueAsString(decryptionProof1) >> "DECRYPTION_PROOF_1"
    objectMapper.writeValueAsString(decryptionProof2) >> "DECRYPTION_PROOF_2"
    objectMapper.writeValueAsString(tally) >> "TALLY"

    // Load the json for comparing outputs
    pubCreds0Json = loadStreamFromResource("publicCredentials-header-and-0-entry.json")
    pubCreds1Json = loadStreamFromResource("publicCredentials-1-entry.json")
    ballotsJson = loadStreamFromResource("ballots.json")
    confirmationsJson = loadStreamFromResource("confirmations.json")
    shuffle0Json = loadStreamFromResource("shuffle0.json")
    shuffle1Json = loadStreamFromResource("shuffle1.json")
    shuffleProof0Json = loadStreamFromResource("shuffleProof0.json")
    shuffleProof1Json = loadStreamFromResource("shuffleProof1.json")
    partialDecryption0Json = loadStreamFromResource("partialDecryption0.json")
    partialDecryption1Json = loadStreamFromResource("partialDecryption1.json")
    partialDecryption2Json = loadStreamFromResource("partialDecryption2.json")
    decryptionProof0Json = loadStreamFromResource("decryptionProof0.json")
    decryptionProof1Json = loadStreamFromResource("decryptionProof1.json")
    decryptionProof2Json = loadStreamFromResource("decryptionProof2.json")
    tallyJson = loadStreamFromResource("tally.json")
  }

  private loadStreamFromResource(String filename) {
    return CharStreams.toString(
        new InputStreamReader(this.class.classLoader.getResourceAsStream(filename), CHARSET))
  }

  def "the data writer should start in unitialized state"() {
    expect:
    electionVerificationDataWriter.state == UNINITIALIZED
  }

  def "initialization should write the object opening and update state"() {
    when:
    electionVerificationDataWriter.initialize()

    then:
    outputStream.toString() == "{" + System.lineSeparator()
    electionVerificationDataWriter.state == INITIALIZED
  }

  def "writePublicParameters should add the public parameters"() {
    when:
    electionVerificationDataWriter.writePublicParameters(publicParameters)

    then:
    outputStream.toString().endsWith("\"publicParameters\": PP_PLACEHOLDER," + System.lineSeparator())
    electionVerificationDataWriter.state == PUBLIC_PARAMETERS_WRITTEN
  }

  def "writeElectionSet should add the election set"() {
    when:
    electionVerificationDataWriter.writeElectionSet(electionSet)

    then:
    outputStream.toString().endsWith("\"electionSet\": ES_PLACEHOLDER," + System.lineSeparator())
    electionVerificationDataWriter.state == ELECTION_SET_WRITTEN
  }

  def "writePrimes should add the primes"() {
    when:
    electionVerificationDataWriter.writePrimes(primes)

    then:
    outputStream.toString().endsWith("\"primes\": PRIMES_PLACEHOLDER," + System.lineSeparator())
    electionVerificationDataWriter.state == PRIMES_WRITTEN
  }

  def "writeGenerators should add the generators"() {
    when:
    electionVerificationDataWriter.writeGenerators(generators)

    then:
    outputStream.toString().endsWith("\"generators\": GENERATORS_PLACEHOLDER," + System.lineSeparator())
    electionVerificationDataWriter.state == GENERATORS_WRITTEN
  }

  def "writePublicKeyParts should add the public key parts"() {
    when:
    electionVerificationDataWriter.writePublicKeyParts(publicKeyParts)

    then:
    outputStream.toString().endsWith("\"publicKeyParts\": PUB_KEY_MAP," + System.lineSeparator())
    electionVerificationDataWriter.state == PUBLIC_KEY_PARTS_WRITTEN
  }

  def "writeElectionOfficerPublicKey should write the election officer public key"() {
    when:
    electionVerificationDataWriter.writeElectionOfficerPublicKey(electionOfficerPublicKey)

    then:
    outputStream.toString().endsWith("\"electionOfficerPublicKey\": EO_KEY," + System.lineSeparator())
    electionVerificationDataWriter.state == ELECTION_OFFICER_KEY_WRITTEN
  }

  def "first call to addPublicCredentials should write the values given but not change the state"() {
    when:
    electionVerificationDataWriter.addPublicCredentials(0, cc0PublicCredParts)

    then: "the values are written to the output stream"
    endsWithIgnoreNewlineStyle(outputStream.toString(), pubCreds0Json)
    electionVerificationDataWriter.state == ELECTION_OFFICER_KEY_WRITTEN
  }

  def "second call to addPublicCredentials should write the values, close the object and change the state"() {
    when:
    electionVerificationDataWriter.addPublicCredentials(1, cc1PublicCredParts)

    then: "the values are written to the output stream and the object is closed"
    endsWithIgnoreNewlineStyle(outputStream.toString(), pubCreds1Json)
    and: "the state is updated"
    electionVerificationDataWriter.state == PUBLIC_CREDENTIALS_WRITTEN
  }

  def "setExpectedBallotKeys should not write to the output stream"() {
    when:
    electionVerificationDataWriter.setExpectedBallotKeys([0, 1])

    then: "no new output should be added"
    endsWithIgnoreNewlineStyle(outputStream.toString(), pubCreds1Json)

    and: "state should be updated"
    electionVerificationDataWriter.state == BALLOT_KEYS_RECEIVED
  }

  def "publishing the expected ballots should write their contents and update the state"() {
    when:
    electionVerificationDataWriter.addBallots([0: ballot0, 1: ballot1])

    then: "the expected data should be written to the stream"
    endsWithIgnoreNewlineStyle(outputStream.toString(), ballotsJson)

    and: "the state should be updated"
    electionVerificationDataWriter.state == BALLOTS_RECEIVED
  }

  def "setExpectedConfirmationKeys should not write to the output stream"() {
    when:
    electionVerificationDataWriter.setExpectedConfirmationKeys([0, 1])

    then: "no new output should be added"
    endsWithIgnoreNewlineStyle(outputStream.toString(), ballotsJson)

    and: "state should be updated"
    electionVerificationDataWriter.state == CONFIRMATION_KEYS_RECEIVED
  }

  def "publishing the expected confirmations should write their contents and update the state"() {
    when:
    electionVerificationDataWriter.addConfirmations([0: confirmation0, 1: confirmation1])

    then: "the expected data should be written to the stream"
    endsWithIgnoreNewlineStyle(outputStream.toString(), confirmationsJson)

    and: "state should be updated"
    electionVerificationDataWriter.state == CONFIRMATIONS_RECEIVED
  }

  def "publishing a shuffle out of order should cache the value, but neither write to the output stream, nor update the state"() {
    when:
    electionVerificationDataWriter.addShuffle(1, shuffle1)

    then: "no data should be written to the output stream"
    endsWithIgnoreNewlineStyle(outputStream.toString(), confirmationsJson)

    and:
    electionVerificationDataWriter.state == CONFIRMATIONS_RECEIVED
  }

  def "publishing the other shuffle should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.addShuffle(0, shuffle0)

    then: "the expected data should be written to the output stream, and then the cached value"
    endsWithIgnoreNewlineStyle(outputStream.toString(), shuffle0Json + shuffle1Json)

    and:
    electionVerificationDataWriter.state == SHUFFLES_RECEIVED
  }

  def "publishing the first shuffle should write to the output stream but not update the state"() {
    when:
    electionVerificationDataWriter.addShuffleProof(0, shuffleProof0)

    then: "the expected data should be written to the output stream"
    endsWithIgnoreNewlineStyle(outputStream.toString(), shuffleProof0Json)

    and:
    electionVerificationDataWriter.state == SHUFFLES_RECEIVED
  }

  def "publishing the last shuffle should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.addShuffleProof(1, shuffleProof1)

    then: "the expected data should be written to the output stream"
    endsWithIgnoreNewlineStyle(outputStream.toString(), shuffleProof1Json)

    and: "the state should be updated"
    electionVerificationDataWriter.state == SHUFFLE_PROOFS_RECEIVED
  }

  def "publishing a partial decryption should write to the output stream but not update the state"() {
    when:
    electionVerificationDataWriter.addPartialDecryptions(0, partialDecryption0)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), partialDecryption0Json)

    and:
    electionVerificationDataWriter.state == SHUFFLE_PROOFS_RECEIVED
  }

  def "publishing the second partial decryption should write to the output stream but not update the state"() {
    when:
    electionVerificationDataWriter.addPartialDecryptions(1, partialDecryption1)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), partialDecryption1Json)

    and:
    electionVerificationDataWriter.state == SHUFFLE_PROOFS_RECEIVED
  }

  def "publishing the last partial decryption should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.addPartialDecryptions(2, partialDecryption2)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), partialDecryption2Json)

    and:
    electionVerificationDataWriter.state == PARTIAL_DECRYPTIONS_RECEIVED
  }

  def "publishing the first decryption proof should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.addDecryptionProof(0, decryptionProof0)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), decryptionProof0Json)

    and:
    electionVerificationDataWriter.state == PARTIAL_DECRYPTIONS_RECEIVED
  }

  def "publishing the second decryption proof should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.addDecryptionProof(1, decryptionProof1)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), decryptionProof1Json)

    and:
    electionVerificationDataWriter.state == PARTIAL_DECRYPTIONS_RECEIVED
  }

  def "publishing the last decryption proof should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.addDecryptionProof(2, decryptionProof2)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), decryptionProof2Json)

    and:
    electionVerificationDataWriter.state == DECRYPTION_PROOFS_RECEIVED
  }

  def "publishing the tally should write to the output stream and update the state"() {
    when:
    electionVerificationDataWriter.writeTally(tally)

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), tallyJson)

    and:
    electionVerificationDataWriter.state == TALLY_WRITTEN
  }

  def "writeEnd should properly close the object"() {
    when:
    electionVerificationDataWriter.writeEnd()

    then:
    endsWithIgnoreNewlineStyle(outputStream.toString(), tallyJson + "}\n")

    and:
    electionVerificationDataWriter.state == DONE
  }

  private static boolean endsWithIgnoreNewlineStyle(String a, String b) {
    return a.replaceAll("\r\n", "\n").endsWith(b.replaceAll("\r\n", "\n"))
  }

}