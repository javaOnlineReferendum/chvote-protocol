/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOTS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATIONS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DECRYPTION_PROOFS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_OFFICER_KEY_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_SET_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.GENERATORS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.INITIALIZED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PARTIAL_DECRYPTIONS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PRIMES_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_CREDENTIALS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_KEY_PARTS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_PARAMETERS_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLES_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLE_PROOFS_RECEIVED
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.TALLY_WRITTEN
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.UNINITIALIZED

import ch.ge.ve.protocol.model.BallotAndQuery
import ch.ge.ve.protocol.model.Confirmation
import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.DecryptionProof
import ch.ge.ve.protocol.model.Decryptions
import ch.ge.ve.protocol.model.ElectionSetForVerification
import ch.ge.ve.protocol.model.Encryption
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.protocol.model.Point
import ch.ge.ve.protocol.model.ShuffleProof
import ch.ge.ve.protocol.model.Tally
import com.fasterxml.jackson.databind.ObjectMapper
import java.nio.file.Files
import java.nio.file.Path
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class ElectionVerificationDataWriterToFilesStepwiseTest extends Specification {

  @Shared
  private Path directory;

  @Shared
  private ObjectMapper mapper;

  @Shared
  private ElectionVerificationDataWriterToFiles writer

  def setupSpec() {
    directory = Files.createTempDirectory(getClass().getSimpleName()).resolve("data")
    mapper = new ObjectMapper()
    writer = new ElectionVerificationDataWriterToFiles(directory, mapper, true)
  }

  def "the data writer should start in uninitialized state"() {
    expect:
    writer.state == UNINITIALIZED
  }

  def "initialize() should create the root directory and update state"() {
    expect:
    !Files.exists(directory)

    when:
    writer.initialize()

    then:
    Files.exists(directory)
    writer.state == INITIALIZED
  }

  def "writePublicParameters(...) should write the public parameters to file"() {
    given:
    def publicParameters = PublicParametersFactory.LEVEL_0.createPublicParameters(2)

    when:
    writer.publicParametersFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writePublicParameters(publicParameters)

    then:
    writer.state == PUBLIC_PARAMETERS_WRITTEN
    Files.exists(writer.publicParametersFilePath)
    read(writer.publicParametersFilePath) == json(publicParameters)
  }

  def "writeElectionSet(...) should write the election set to file"() {
    given:
    def electionSet = new ElectionSetForVerification([], [], [], 1L, 1, [1] as int[])

    when:
    writer.electionSetFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writeElectionSet(electionSet)

    then:
    writer.state == ELECTION_SET_WRITTEN
    Files.exists(writer.electionSetFilePath)
    read(writer.electionSetFilePath) == json(electionSet)
  }

  def "writePrimes(...) should write the primes to file"() {
    given:
    def primes = [BigInteger.ONE, BigInteger.TEN, BigInteger.ZERO]

    when:
    writer.primesFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writePrimes(primes)

    then:
    writer.state == PRIMES_WRITTEN
    Files.exists(writer.primesFilePath)
    read(writer.primesFilePath) == json(primes)
  }

  def "writeGenerators(...) should write the generators to file"() {
    given:
    def generators = [BigInteger.ONE, BigInteger.TEN, BigInteger.ZERO]

    when:
    writer.generatorsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writeGenerators(generators)

    then:
    writer.state == GENERATORS_WRITTEN
    Files.exists(writer.generatorsFilePath)
    read(writer.generatorsFilePath) == json(generators)
  }

  def "writePublicKeyParts(...) should write the public key parts to file"() {
    given:
    def encryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)
    def publicKeyParts = [
        0: new EncryptionPublicKey(FIVE, encryptionGroup),
        1: new EncryptionPublicKey(THREE, encryptionGroup)
    ]

    when:
    writer.publicKeyPartsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writePublicKeyParts(publicKeyParts)

    then:
    writer.state == PUBLIC_KEY_PARTS_WRITTEN
    Files.exists(writer.publicKeyPartsFilePath)
    read(writer.publicKeyPartsFilePath) == json(publicKeyParts)
  }

  def "writeElectionOfficerPublicKey(...) should write the election officer public key to file"() {
    given:
    def publicKey = new EncryptionPublicKey(FIVE, new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR))

    when:
    writer.electionOfficerPublicKeyFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writeElectionOfficerPublicKey(publicKey)

    then:
    writer.state == ELECTION_OFFICER_KEY_WRITTEN
    Files.exists(writer.electionOfficerPublicKeyFilePath)
    read(writer.electionOfficerPublicKeyFilePath) == json(publicKey)
  }

  def "addPublicCredentials(...) should write the public credentials to file"() {
    given:
    def publicCredentials = [
        new Point(BigInteger.ONE, BigInteger.TEN),
        new Point(BigInteger.TEN, BigInteger.ONE)
    ]

    when:
    writer.publicCredentialsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.addPublicCredentials(0, [0: publicCredentials[0]])
    writer.addPublicCredentials(1, [0: publicCredentials[1]])

    then:
    writer.state == PUBLIC_CREDENTIALS_WRITTEN
    Files.exists(writer.publicCredentialsFilePath)
    read(writer.publicCredentialsFilePath) == json([0: [publicCredentials[0]], 1: [publicCredentials[1]]])
  }

  def "addBallots(...) should write the ballots to file"() {
    given:
    def ballot = new BallotAndQuery(BigInteger.ZERO, [], new NonInteractiveZkp([], []))

    when:
    writer.ballotsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.setExpectedBallotKeys([0])
    writer.addBallots([0: ballot])

    then:
    writer.state == BALLOTS_RECEIVED
    Files.exists(writer.ballotsFilePath)
    read(writer.ballotsFilePath) == json([0: ballot])
  }

  def "addConfirmations(...) should write the confirmations to file"() {
    given:
    def confirmation = new Confirmation(BigInteger.ZERO, new NonInteractiveZkp([], []))

    when:
    writer.confirmationsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.setExpectedConfirmationKeys([0])
    writer.addConfirmations([0: confirmation])

    then:
    writer.state == CONFIRMATIONS_RECEIVED
    Files.exists(writer.confirmationsFilePath)
    read(writer.confirmationsFilePath) == json([0: confirmation])
  }

  def "addShuffle(...) should write the shuffles to file"() {
    given:
    def shuffles = [
        0: [new Encryption(BigInteger.ZERO, BigInteger.ZERO)],
        1: [new Encryption(BigInteger.ONE, BigInteger.ONE)]
    ]

    when:
    writer.shufflesFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.addShuffle(0, shuffles[0])
    writer.addShuffle(1, shuffles[1])

    then:
    writer.state == SHUFFLES_RECEIVED
    Files.exists(writer.shufflesFilePath)
    read(writer.shufflesFilePath) == json(shuffles)
  }

  def "addShuffleProof(...) should write the shuffle proofs to file"() {
    given:
    def shuffleProofs = [
        0: new ShuffleProof(
            new ShuffleProof.T(BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, [BigInteger.ZERO, BigInteger.ZERO], [BigInteger.ZERO]),
            new ShuffleProof.S(BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, [BigInteger.ZERO], [BigInteger.ZERO]),
            [], []
        ),
        1: new ShuffleProof(
            new ShuffleProof.T(BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, [BigInteger.ONE, BigInteger.ONE], [BigInteger.ONE]),
            new ShuffleProof.S(BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, [BigInteger.ONE], [BigInteger.ONE]),
            [], []
        )
    ]

    when:
    writer.shuffleProofsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.addShuffleProof(0, shuffleProofs[0])
    writer.addShuffleProof(1, shuffleProofs[1])

    then:
    writer.state == SHUFFLE_PROOFS_RECEIVED
    Files.exists(writer.shuffleProofsFilePath)
    read(writer.shuffleProofsFilePath) == json(shuffleProofs)
  }

  def "addPartialDecryptions(...) should write the partial decryptions to file"() {
    given:
    def decryptions = [
        0: new Decryptions([BigInteger.ZERO]),
        1: new Decryptions([BigInteger.ONE]),
        2: new Decryptions([BigInteger.TEN])
    ]

    when:
    writer.partialDecryptionsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.addPartialDecryptions(0, decryptions[0])
    writer.addPartialDecryptions(1, decryptions[1])
    writer.addPartialDecryptions(2, decryptions[2])

    then:
    writer.state == PARTIAL_DECRYPTIONS_RECEIVED
    Files.exists(writer.partialDecryptionsFilePath)
    read(writer.partialDecryptionsFilePath) == json(decryptions)
  }

  def "addDecryptionProof(...) should write the decryption proofs to file"() {
    given:
    def decryptionProofs = [
        0: new DecryptionProof([], BigInteger.ZERO),
        1: new DecryptionProof([], BigInteger.ONE),
        2: new DecryptionProof([], BigInteger.TEN)
    ]

    when:
    writer.decryptionProofsFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.addDecryptionProof(0, decryptionProofs[0])
    writer.addDecryptionProof(1, decryptionProofs[1])
    writer.addDecryptionProof(2, decryptionProofs[2])

    then:
    writer.state == DECRYPTION_PROOFS_RECEIVED
    Files.exists(writer.decryptionProofsFilePath)
    read(writer.decryptionProofsFilePath) == json(decryptionProofs)
  }

  def "writeTally(...) should write the tally to file"() {
    given:
    def tally = new Tally(Collections.singletonMap(new CountingCircle(0, "#1", "cc"), []))

    when:
    writer.tallyFilePath

    then:
    thrown(IllegalStateException)

    when:
    writer.writeTally(tally)

    then:
    writer.state == TALLY_WRITTEN
    Files.exists(writer.tallyFilePath)
    read(writer.tallyFilePath) == json(tally)
  }

  def "writeEnd() should not throw an exception"() {
    when:
    writer.writeEnd()

    then: "no exception is thrown"
  }

  private String read(Path path) {
    return path.getText().replaceAll("\\s", "")
  }

  private String json(Object o) {
    return mapper.writeValueAsString(o)
  }
}
