/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import ch.ge.ve.protocol.model.Tally;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import java.util.Objects;

/**
 * Container object that holds reference on objects related to generated votes cast.
 */
public final class GeneratedVotesData {

  private final Map<Integer, VotePerformanceStats> performanceStats;
  private final Tally expectedTally;

  public GeneratedVotesData(Map<Integer, VotePerformanceStats> performanceStats, Tally expectedTally) {
    this.performanceStats = ImmutableMap.copyOf(performanceStats);
    this.expectedTally = Objects.requireNonNull(expectedTally, "Expected tally must be provided");
  }

  public Map<Integer, VotePerformanceStats> getPerformanceStats() {
    return performanceStats;
  }

  public Tally getExpectedTally() {
    return expectedTally;
  }
}
