/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.acknowledgement.repository

import ch.ge.ve.config.TestConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import org.springframework.test.context.transaction.TransactionalTestExecutionListener
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

@Transactional
@ContextConfiguration(classes = TestConfig.class)
@TestExecutionListeners([
    DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class
])
class AcknowledgementPendingEventRepositoryIT extends Specification {

  @Autowired
  AcknowledgementPendingEventJpaRepository repository

  def "countByEventId should retrieve the expected count"() {
    given:
    // The test data in import.sql

    expect:
    repository.countByEventId("1_1") == 1
    repository.countByEventId("1_3") == 2
  }

  def "deleteByProtocolId should delete the expected events"() {
    given:
    // The test data in import.sql

    expect:
    repository.count() == 12

    when:
    repository.deleteByProtocolId("1")

    then:
    repository.count() == 6
  }

  def "deleteByEventIdAndAcknowledgementPartIndex should delete the expected event"() {
    given:
    // The test data in import.sql

    expect:
    repository.countByEventId("1_1") == 1
    repository.countByEventId("1_3") == 2

    when:
    repository.deleteByEventIdAndAcknowledgementPartIndex("1_1", null)
    repository.deleteByEventIdAndAcknowledgementPartIndex("1_3", "CC_0")

    then:
    repository.countByEventId("1_1") == 0
    repository.countByEventId("1_3") == 1
  }
}
