/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.model.ElectionVerificationData;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to write the election verification data to a file
 */
public class ElectionVerificationDataWriter {
  private static final Logger log = LoggerFactory.getLogger(ElectionVerificationDataWriter.class);
  private final ObjectMapper jsonObjectMapper;

  public ElectionVerificationDataWriter(ObjectMapper jsonObjectMapper) {
    this.jsonObjectMapper = jsonObjectMapper;
  }

  /**
   * Writes the election verification data to a file
   *
   * @param electionVerificationData data to be written
   * @param outputPath               path of the new file
   */
  public void writeElectionVerificationData(ElectionVerificationData electionVerificationData, Path outputPath) {
    try (OutputStream out = Files.newOutputStream(outputPath, StandardOpenOption.CREATE)) {
      jsonObjectMapper.writeValue(out, electionVerificationData);
      log.info("Verification data written at {}", outputPath);
    } catch (IOException e) {
      log.error("Failed to write the verification data file", e);
    }
    if (log.isInfoEnabled()) {
      log.info("File has size: {}", outputPath.toFile().length());
    }
  }
}
