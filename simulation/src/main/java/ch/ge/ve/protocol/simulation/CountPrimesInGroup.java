/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.config.BigIntegerAsBase64Deserializer;
import ch.ge.ve.config.BigIntegerAsBase64Serializer;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.model.ElectionSetAndVoters;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import java.math.BigInteger;
import java.security.Security;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Utility class to assert the bit length of a product of primes in a group
 */
public class CountPrimesInGroup {
  private static final String ZEROES_70    = "0000000000000000000000000000000000000000000000000000000000000000000000";
  private static final String p_string     = "8000000000000000000000000000000000000000000000000000000000000000000000" +
                                             ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 +
                                             "00000000000000000AD3AF";
  private static final String q_string     = "4000000000000000000000000000000000000000000000000000000000000000000000" +
                                             ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 +
                                             "00000000000000000569D7";
  private static final String p_hat_string = "8000000000000000000000000000000000000000000000000000000000000000000000" +
                                             ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 + ZEROES_70 +
                                             "0000000000000000000000000000000002B3000000000000000000000000000010AE7A" +
                                             "58BB039DF1DD000003FC4F";
  private static final String q_hat_string = "800000000000000000000000000000000000000000000000000000BD";
  private static final String g_hat_string = "8FC32AED89182BD1CAC46EA9CDBECF57DB18D8748E039355CCBC90DEDA214943780680" +
                                             "CCD7D379440E833E03681AD5C93C3CCB3909333D2DC500688237C4D0623703823F026F" +
                                             "CD67103BA49EE2D3B3DDFAC5B797636FFC4369177FFA357B722935B2EF3B2E3F1DFEA7" +
                                             "36903F76927794D071A723F79666EE23FF0EDE87AFB3F60792CFFC7078CB96D8A23066" +
                                             "C8C412813F5943CF9E98B8FE3E21A0A8F241A830BF39C16BB8F2F21D53EB91F30262A8" +
                                             "6A043C5DF1167CB748B6EACC5946D612EB8DFEB454E0B1289A7CF66F2940C83CD46118" +
                                             "B37B949905AEAF315F537B5B54BF75138603D54BCC4C2D6E72C0E7DD50B5925417E5C2" +
                                             "77E411B9394FB2FDAC0DF";
  private static final Logger log          = LoggerFactory.getLogger(CountPrimesInGroup.class);

  public static void main(String[] args) throws NotEnoughPrimesInGroupException {
    Security.addProvider(new BouncyCastleProvider());

    Jackson2ObjectMapperBuilder objectMapperBuilder = Jackson2ObjectMapperBuilder.json();
    objectMapperBuilder.serializers(new BigIntegerAsBase64Serializer());
    objectMapperBuilder.deserializers(new BigIntegerAsBase64Deserializer());
    objectMapperBuilder.indentOutput(true);

    ObjectMapper mapper = objectMapperBuilder.build();

    ElectionSetFactory esf = ElectionSetFactory.EXTREME;
    ElectionSetAndVoters electionSetAndVoters = esf.createElectionSetAndVoters(3800, mapper, 2);
    ElectionSetWithPublicKey electionSet = electionSetAndVoters.getElectionSet();
    List<Election> elections = new ArrayList<>(electionSet.getElections());
    List<CountingCircle> countingCircles =
        electionSetAndVoters.getVoters().parallelStream()
                            .map(Voter::getCountingCircle).distinct().collect(Collectors.toList());

    BigInteger p = new BigInteger(p_string, 16);
    BigInteger q = new BigInteger(q_string, 16);
    BigInteger g = BigInteger.valueOf(4L);
    BigInteger h = BigInteger.valueOf(9L);
    EncryptionGroup encryptionGroup = new EncryptionGroup(p, q, g, h);

    BigInteger p_hat = new BigInteger(p_hat_string, 16);
    BigInteger q_hat = new BigInteger(q_hat_string, 16);
    BigInteger g_hat = new BigInteger(g_hat_string, 16);
    IdentificationGroup identificationGroup = new IdentificationGroup(p_hat, q_hat, g_hat);

    Hash hash = new Hash("BLAKE2B-256", "BC", 32);
    int primesNeeded = electionSet.getCandidates().size() + countingCircles.size();
    GeneralAlgorithms generalAlgorithms = new GeneralAlgorithms(hash, encryptionGroup, identificationGroup,
                                                                PrimesCache.populate(primesNeeded, encryptionGroup));

    List<BigInteger> primes = generalAlgorithms.getPrimes(primesNeeded);

    Stopwatch stopwatch = Stopwatch.createStarted();
    elections.sort(Comparator.comparingInt(Election::getNumberOfCandidates).reversed());
    int n_max = 0;
    List<BigInteger> maxFactors = new ArrayList<>();
    for (Election election : elections) {
      n_max += election.getNumberOfCandidates();
      int k_max = election.getNumberOfSelections();
      BigInteger electionMaxFactor = primes.stream()
                                           .limit(n_max)
                                           .skip((long) n_max - k_max)
                                           .reduce(BigInteger.ONE, BigInteger::multiply);
      maxFactors.add(electionMaxFactor);
    }

    BigInteger max_product = maxFactors.stream()
                                       .reduce(BigInteger.ONE, BigInteger::multiply)
                                       .multiply(primes.get(n_max + countingCircles.size() - 1));

    Integer maxLength = max_product.bitLength();

    stopwatch.stop();

    if (log.isInfoEnabled()) {
      log.info(String.format("Bit length of the maxproduct factor for this election set is %s",
                             maxLength));
      log.info(String.format("Execution time: %dms", stopwatch.elapsed(TimeUnit.MILLISECONDS)));
    }
  }
}
