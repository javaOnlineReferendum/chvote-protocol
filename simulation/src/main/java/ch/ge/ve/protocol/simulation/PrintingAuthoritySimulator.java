/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VotingCardPreparationAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.simulation.exception.SimulationRuntimeException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Simulation class for the Printing Authority.
 */
class PrintingAuthoritySimulator {
  private final String                          protocolId;
  private final IdentificationPrivateKey        decryptionKey;
  private final BulletinBoard                   bulletinBoard;
  private final List<BigInteger>                controlComponentPublicKeys = new ArrayList<>();
  private final List<VoterSimulator>            voterSimulators            = new ArrayList<>();
  private final VotingCardPreparationAlgorithms votingCardPreparationAlgorithms;
  private final ChannelSecurityAlgorithms       channelSecurityAlgorithms;
  private final ObjectMapper                    jsonObjectMapper;

  PrintingAuthoritySimulator(String protocolId,
                             IdentificationPrivateKey decryptionKey, BulletinBoard bulletinBoard,
                             VotingCardPreparationAlgorithms votingCardPreparationAlgorithms,
                             ChannelSecurityAlgorithms channelSecurityAlgorithms, ObjectMapper jsonObjectMapper) {
    this.protocolId = protocolId;
    this.decryptionKey = decryptionKey;
    this.bulletinBoard = bulletinBoard;
    this.votingCardPreparationAlgorithms = votingCardPreparationAlgorithms;
    this.channelSecurityAlgorithms = channelSecurityAlgorithms;
    this.jsonObjectMapper = jsonObjectMapper;
  }

  void setControlComponentPublicKeys(List<BigInteger> controlComponentPublicKeys) {
    Preconditions.checkState(this.controlComponentPublicKeys.isEmpty(),
                             "Control components certificates may not be updated once set");
    this.controlComponentPublicKeys.addAll(controlComponentPublicKeys);
  }

  void setVoterSimulators(List<VoterSimulator> voterSimulators) {
    Preconditions.checkState(this.voterSimulators.isEmpty(), "Voter simulators may not be updated once set");
    this.voterSimulators.addAll(voterSimulators);
  }

  void print(List<byte[]> printingAuthorityVoterCredentials) {
    final PublicParameters publicParameters = bulletinBoard.getPublicParameters(protocolId);
    Preconditions.checkState(controlComponentPublicKeys.size() == publicParameters.getS());
    ElectionSetWithPublicKey electionSet = bulletinBoard.getElectionSet(protocolId);
    Preconditions.checkArgument(printingAuthorityVoterCredentials.size() == publicParameters.getS());

    List<List<SecretVoterData>> voterDataMatrix =
        IntStream.range(0, printingAuthorityVoterCredentials.size()).mapToObj(i -> {
          try {
            byte[] signEncryptedCreds = printingAuthorityVoterCredentials.get(i);
            BigInteger signingPublicKey = controlComponentPublicKeys.get(i);

            String serializedCreds = channelSecurityAlgorithms
                .decryptAndVerify(decryptionKey.getPrivateKey(), signingPublicKey, deserializeEncryptedMessage(signEncryptedCreds));

            return deserializeSecretVoterDataList(serializedCreds);
          } catch (ChannelSecurityException e) {
            throw new SimulationRuntimeException("Failed to decrypt / verify the signEncrypted credentials", e);
          }
        }).collect(Collectors.toList());
    List<VotingCard> sheets = votingCardPreparationAlgorithms.getVotingCards(electionSet, voterDataMatrix);
    for (int i = 0; i < sheets.size(); i++) {
      voterSimulators.get(i).sendCodeSheet(sheets.get(i));
    }
  }

  private EncryptedMessageWithSignature deserializeEncryptedMessage(byte[] cipher) {
    try {
      return jsonObjectMapper.readValue(cipher, EncryptedMessageWithSignature.class);
    } catch (IOException e) {
      throw new SimulationRuntimeException("Failed to deserialize the cipher with signature", e);
    }
  }

  private List<SecretVoterData> deserializeSecretVoterDataList(String serializedCreds) {
    try {
      return jsonObjectMapper.readValue(serializedCreds, new TypeReference<List<SecretVoterData>>() {
      });
    } catch (IOException e) {
      throw new SimulationRuntimeException("Failed to deserialize the secret voter data list", e);
    }
  }
}
