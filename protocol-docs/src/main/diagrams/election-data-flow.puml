@startuml
actor "Election officer" as officer
participant "Printer files tool" as PFT
participant "File signing tool" as FST
participant "Backoffice" as BOH
actor "Printer" as printer

participant "Voting platform" as VP
actor "Voter" as voter
actor "Electoral board" as board
actor "IT Support" as IT

actor "2nd election officer" as officer2

participant "Offline results generator" as ORG

actor "Swiss Post" as post

entity "Printer files\nencryption key pair\nfor test cards" as PF_KP_tests
entity "Printer files\nencryption key pair\nfor real cards" as PF_KP_real
entity "Printer files\nencryption key pair\nfor IT support cards" as PF_KP_support
entity "Electoral board\nkey pair" as EB_KP

actor "Verifier" as verifier
participant "Verification tool" as VT

== Election officer keys generation ==
officer -> PFT: create printer files encryption key pair for test cards
PFT -> PF_KP_tests: create
officer <-- PFT: printer files encryption key pair for test cards
|||
officer -> FST: printer files encryption public key for test cards
officer <-- FST: printer files encryption public key for test cards + signature
|||
officer -> BOH: printer files encryption public key for test cards + signature

== Printers keys generation ==
printer -> PFT: create printer files encryption key pair for real cards
PFT -> PF_KP_real: create
printer <-- PFT: printer files encryption key pair for real cards
|||
printer -> FST: printer files encryption public key for real cards
printer <-- FST: printer files encryption public key for real cards + signature
printer -> BOH: printer files encryption public key for real cards + signature

== IT Support keys generation ==
IT -> PFT: create printer files encryption key pair for support cards
PFT -> PF_KP_support: create
IT <-- PFT: printer files encryption key pair for support cards
|||
IT -> FST: printer files encryption key pair for support cards
IT <-- FST: printer files encryption key pair for support cards + signature
IT -> BOH: printer files encryption key pair for support cards + signature
note left: The IT support cards are automatically decrypted by the BOH,\nthus needing the private key.


== Electoral board keys generation ==
officer -> ORG: create key pair with passphrases
ORG -> EB_KP: create
board -> ORG: enter passphrases
officer <-- ORG: electoral board key pair protected by passphrases

== Configure the operation ==

officer -> FST: operation repository (eCH-0157 or eCH-0159)
officer <-- FST: operation repository (eCH-0157 or eCH-0159) + signature
|||
officer -> FST: logistics data
officer <-- FST: logistics data + signature
|||
officer -> FST: import electoral board public key
officer <-- FST: electoral board public key + signature

|||
officer -> BOH: import operation repository (eCH-0157 or eCH-0159) + signature
officer -> BOH: import logistics data + signature
officer -> BOH: import electoral board public key + signature
officer -> BOH: import web assets (pdf, images, ...)
|||
alt certification step
officer -> BOH: get printer files encryption public key for test cards
officer <-- BOH: encryption public key
else live step
officer -> BOH: get printer files encryption public key for real cards
officer <-- BOH: encryption public key
end
officer -> PFT: encrypt register eCH-0045 (encryption public key)
officer <-- PFT: register eCH-0045 with encrypted personal data
|||
officer -> FST: register eCH-0045 with encrypted personal data
officer <-- FST: register eCH-0045 with encrypted personal data + signature
|||
officer -> BOH: register eCH-0045 with encrypted personal data + signature
|||
officer -> BOH: update platform configuration
BOH -> VP: electoral board public key + signature
BOH -> VP: return codes encryption public key for tests + signature
BOH -> VP: return codes encryption public key for real cards + signature
BOH -> VP: configuration elements
BOH -> VP: register eCH-0045 with encrypted personal data + signature


== Generate the printer files ==

officer -> BOH: generate the printer files
BOH -> VP: generate the printer files
BOH <-- VP: Real printer files with encrypted personal data + signature
BOH <-- VP: Test and control printer files + signature
BOH <-- VP: IT support cards + signature
BOH <-- VP: Printer files report
|||
alt certification step
officer -> BOH: get the test printer files
officer <-- BOH: Test printer files + signature
else live step
printer -> BOH: get the real printer files
printer <-- BOH: Real printer files + signature
end
|||
officer -> BOH: get the printer files report
officer <-- BOH: Printer files report
|||
printer -> BOH: get the printer files report
printer <-- BOH: Printer files report
|||
IT -> BOH: get the IT support cards
IT <-- BOH: IT support cards

== Print and send the voting cards ==
printer -> PFT: decrypt the printer files
printer <-- PFT: xml printer file
printer -> printer: print the voting cards
printer -> post: deposit the voting cards
post -> voter: distribute the voting card

== Vote ==
voter -> VP: identify
voter <-- VP: election data
voter -> VP: send ballot and authentication
voter <-- VP: verification codes
voter -> VP: send confirmation code
voter <-- VP: finalisation code

== Decryption and tallying ==
officer -> VP: ask to decrypt the ballot box
officer2 -> VP: authorize the ballot box decryption
VP -> VP: mix the ballot box
VP -> VP: partially decrypt the ballot box
officer -> VP: get the ballot box
officer <-- VP: ballot box + signature
note right: The ballot box is a package containing every messages and proofs\ngenerated by the authorities.\nIt is still encrypted with the electoral board key at this step.
|||
officer -> FST: verify the signature (ballot box)
officer -> ORG: decrypt the ballot box (ballot box, electoral board key pair)
board -> ORG: enter passphrases
officer <-- ORG: eCH-0110 results + proofs of decryption ?
|||
officer -> verifier: give the ballot box
verifier -> VT: verify the proofs up to the partial decryption
|||
officer -> verifier: give the results and proofs of decryption
verifier -> VT: verify the last proofs
|||
alt Mandate
officer -> BOH: import the eCH-0110 results
end
@enduml