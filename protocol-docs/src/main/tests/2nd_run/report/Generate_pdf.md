# Generating a pdf out of the markdown reports

## Prerequisites
- Linux OS
- install asciidoctor and asciidoctor-pdf: 
```
apt-get install asciidoctor
gem install asciidoctor-pdf --pre
```

## Generate the pdf
Run the following command to transform a markdown document into a pdf:
```
asciidoctor-pdf <document>.adoc
``` 