/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.VotesCastCountRequestEvent;
import ch.ge.ve.event.VotesCastCountResponseEvent;
import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Bulletin board votes cast count request events listener.
 */
@Component
public class BulletinBoardVotesCastCountListener {

  private static final Logger log = LoggerFactory.getLogger(BulletinBoardVotesCastCountListener.class);

  private final BulletinBoard bulletinBoard;
  private final EventBus      eventBus;

  @Autowired
  public BulletinBoardVotesCastCountListener(BulletinBoard bulletinBoard, EventBus eventBus) {
    this.bulletinBoard = bulletinBoard;
    this.eventBus = eventBus;
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(
              value = Channels.VOTES_CAST_COUNT_REQUEST + "-bulletinBoard",
              arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
              durable = "true"),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                               value = Channels.VOTES_CAST_COUNT_REQUEST)
      )
  )
  public void processVotesCastCountRequestEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                VotesCastCountRequestEvent event) {
    log.info("Received votes cast count request...");
    eventBus.publish(Channels.VOTES_CAST_COUNT_RESPONSE, protocolId, Endpoints.BULLETIN_BOARD,
                     new VotesCastCountResponseEvent(bulletinBoard.countVotesCast(protocolId)));
  }
}
