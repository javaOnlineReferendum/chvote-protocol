/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.bulletinboard;

import ch.ge.ve.protocol.bulletinboard.model.TallyData;
import ch.ge.ve.protocol.core.model.ShufflesAndProofs;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * This interface defines the contract for the bulletin board.
 */
public interface BulletinBoard {
  /**
   * Stores the election event's public parameters.
   *
   * @param protocolId       the protocol instance Id.
   * @param publicParameters the election event's public parameters.
   *
   * @throws NullPointerException  if {@code publicParameters} is {@code null}.
   * @throws IllegalStateException if different public parameters have already been stored.
   */
  void storePublicParameters(String protocolId, PublicParameters publicParameters);

  /**
   * Returns the election event's public parameters or {@code null} if they have not been stored yet.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the election event's public parameters or {@code null} if they have not been stored yet.
   */
  PublicParameters getPublicParameters(String protocolId);

  /**
   * Stores the given encryption (public) key part.
   *
   * @param protocolId the protocol instance Id.
   * @param j          the control component's index whose key is being stored.
   * @param publicKey  the encryption (public) key part.
   *
   * @throws NullPointerException      if {@code publicKey} is {@code null}.
   * @throws IllegalStateException     if the election event's public parameters have not yet been stored.
   * @throws IndexOutOfBoundsException if {@code j} is negative or is not less than the number of control components.
   */
  void storeKeyPart(String protocolId, int j, EncryptionPublicKey publicKey);

  /**
   * Stores the election officer public key
   *
   * @param protocolId               the protocol instance Id.
   * @param electionOfficerPublicKey the election officer public key
   *
   * @throws NullPointerException  if {@code electionOfficerPublicKey} is {@code null}
   * @throws IllegalStateException if a different election officer public key has already been stored
   */
  void storeElectionOfficerPublicKey(String protocolId, EncryptionPublicKey electionOfficerPublicKey);

  /**
   * Returns all the stored encryption (public) key parts.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return all the stored encryption (public) key parts.
   *
   * @throws IllegalStateException if the election event's public parameters have not yet been stored or if not all
   *                               control components have published their encryption key part yet or if the election
   *                               officer public key has not been stored yet
   */
  List<EncryptionPublicKey> getPublicKeyParts(String protocolId);

  /**
   * Stores the election set.
   *
   * @param protocolId  the protocol instance Id.
   * @param electionSet the election set.
   *
   * @throws NullPointerException if {@code electionSet} is {@code null}.
   */
  void storeElectionSet(String protocolId, ElectionSetWithPublicKey electionSet);

  /**
   * Stores voters. (May be called multiple times with disjoint subsets of voters).
   *
   * @param protocolId the protocol instance Id.
   * @param voters     some voters
   */
  void storeVoters(String protocolId, List<Voter> voters);

  /**
   * Returns the election set.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the election set.
   *
   * @throws IllegalStateException if the election set has not been stored yet.
   */
  ElectionSetWithPublicKey getElectionSet(String protocolId);

  /**
   * Stores the primes used for representing the voters' choices on the encrypted ballot
   *
   * @param protocolId the protocol instance Id.
   * @param primes     the list of primes to be stored
   *
   * @throws IllegalStateException if a different list of primes has been set beforehand
   */
  void storePrimes(String protocolId, List<BigInteger> primes);

  /**
   * Stores the given public credentials part.
   *
   * @param protocolId        the protocol instance Id.
   * @param j                 the control component's index associated to the public credentials part being stored.
   * @param publicCredentials the public credentials part.
   *
   * @throws NullPointerException      if {@code publicCredentials} is {@code null}.
   * @throws IllegalStateException     if the election event's public parameters have not yet been stored.
   * @throws IndexOutOfBoundsException if {@code j} is negative or is not less than the number of control components.
   */
  void storePublicCredentials(String protocolId, int j, Map<Integer, Point> publicCredentials);

  /**
   * Stores the given ballot into the bulletin board
   *
   * @param protocolId     the protocol instance Id.
   * @param voterIndex     the voter's index.
   * @param ballotAndQuery the voter's ballot and verification codes oblivious transfer query.
   *
   * @throws NullPointerException if one of the arguments is {@code null}.
   */
  void storeBallot(String protocolId, Integer voterIndex, BallotAndQuery ballotAndQuery);

  /**
   * Stores a ballot's confirmation into the bulletin board
   *
   * @param protocolId   the protocol instance Id.
   * @param voterIndex   the voter's index.
   * @param confirmation the voter's confirmation data.
   *
   * @throws NullPointerException if one of the arguments is {@code null}.
   */
  void storeConfirmation(String protocolId, Integer voterIndex, Confirmation confirmation);

  /**
   * Returns the current number of confirmed votes.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the current number of confirmed votes.
   *
   * @throws NullPointerException if {@code protocolId} is {@code null}.
   */
  long countVotesCast(String protocolId);

  /**
   * Stores the generators used for the shuffle proof generation
   *
   * @param protocolId the protocol instance Id.
   * @param generators the generators used
   *
   * @throws IllegalStateException if another value has already been set for the generators
   */
  void storeGenerators(String protocolId, List<BigInteger> generators);

  /**
   * Stores the given shuffles and their associated proofs.
   *
   * @param protocolId   the protocol instance Id.
   * @param j            the index of the control component who contributed the shuffle.
   * @param shuffle      the shuffle
   * @param shuffleProof the shuffle proof
   *
   * @throws NullPointerException      if one of the arguments is {@code null}.
   * @throws IllegalStateException     if the public parameters have not been stored yet.
   * @throws IndexOutOfBoundsException if {@code j} is negative or is not less than the number of control components.
   * @throws IllegalArgumentException  if the {@code j} does not match the number of published shuffles.
   */
  void storeShuffleAndProof(String protocolId, int j, List<Encryption> shuffle, ShuffleProof shuffleProof);

  /**
   * Returns all the published shuffles and associated proofs.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return all the published shuffles and associated proofs.
   *
   * @throws IllegalStateException if the election event's public parameters have not yet been published or if not all
   *                               control components have published their shuffle and associated proofs.
   */
  ShufflesAndProofs getShufflesAndProofs(String protocolId);

  /**
   * Stores the given partial decryptions and its associated proof.
   *
   * @param protocolId         the protocol instance Id.
   * @param j                  the index of the control component publishing the partial decryptions
   * @param partialDecryptions the partial decryptions.
   * @param decryptionProof    the associated decryption proof.
   *
   * @throws NullPointerException      if one of the arguments is {@code null}.
   * @throws IllegalStateException     if the election event's public parameters have not yet been stored.
   * @throws IndexOutOfBoundsException if {@code j} is negative or is not less than the number of control components.
   * @throws IllegalArgumentException  if a different partial decryption contribution from the control component indexed
   *                                   {@code j} has already been stored.
   */
  void storePartialDecryptionsAndProof(String protocolId, int j,
                                       Decryptions partialDecryptions, DecryptionProof decryptionProof);

  /**
   * Returns all the data required for the tallying of the votes.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return all the data required for the tallying of the votes.
   *
   * @throws IllegalStateException if not all control components have published their partial decryption yet.
   */
  TallyData getTallyData(String protocolId);

  /**
   * Stores the votes' tally.
   *
   * @param protocolId the protocol instance Id.
   * @param tally      the votes' tally to store.
   *
   * @throws NullPointerException  if {@code tally} is {@code null}.
   * @throws IllegalStateException if not all control components have published their partial decryption yet.
   */
  void storeTally(String protocolId, Tally tally);

  /**
   * Retrieves the elements of the election set necessary to the verification of the protocol
   *
   * @param protocolId the protocol instance identifier
   *
   * @return a view of the election set containing the elements necessary for the verification
   *
   * @throws IllegalStateException if the election set or the voters list have not been set yet
   */
  ElectionSetForVerification getElectionSetForVerification(String protocolId);

  /**
   * Retrieves the list of primes used for representing the voters' choices
   *
   * @param protocolId the protocol instance identifier
   *
   * @return the list of primes in increasing value order
   *
   * @throws IllegalStateException if the primes have not been set beforehand
   */
  List<BigInteger> getPrimes(String protocolId);

  /**
   * Retrieves the list of generators used for shuffle proofs for the provided protocol id
   *
   * @param protocolId the protocol instance identifier
   *
   * @return the list of generators used in the shuffle proofs
   *
   * @throws IllegalStateException if the generators have not been defined beforehand
   */
  List<BigInteger> getGenerators(String protocolId);

  /**
   * Retrieve the public key part of the control components, indexed by control component id
   *
   * @param protocolId the protocol instance identifier
   *
   * @return a map of the control components' encryption public keys, by control component id
   */
  Map<Integer, EncryptionPublicKey> getPublicKeyMap(String protocolId);

  /**
   * Retrieves the election officer public key for the provided protocol id
   *
   * @param protocolId the the protocol instance identifier
   *
   * @return the election officer's public key
   *
   * @throws IllegalStateException if the election officer public key has not been defined beforehand
   */
  EncryptionPublicKey getElectionOfficerPublicKey(String protocolId);

  /**
   * Retrieves the public credentials generated by the control component at the given index for the voters with given
   * ids
   *
   * @param protocolId the protocol instance identifier
   * @param ccIndex    the control component index
   * @param voterIds   the voter indices
   *
   * @return a map from voter id to public credentials
   */
  Map<Integer, Point> getPublicCredentialsParts(String protocolId, int ccIndex, List<Integer> voterIds);

  /**
   * Retrieves the list of keys (i.e. voter ids) for which ballots have been received
   *
   * @param protocolId the protocol instance identifier
   *
   * @return a list of the ids of the voters having submitted a ballot
   */
  List<Integer> getBallotKeys(String protocolId);

  /**
   * Retrieves a map of ballot by voter id for the given parameters
   *
   * @param protocolId the protocol instance identifier
   * @param voterIds   the voter indices
   *
   * @return for each provided voter id, the corresponding cast ballot (encrypted)
   */
  Map<Integer, BallotAndQuery> getBallots(String protocolId, List<Integer> voterIds);

  /**
   * Retrieves the list of keys (i.e. voter ids) for which confirmations have been received
   *
   * @param protocolId the protocol instance identifier
   *
   * @return a list of the ids of the voters having submitted a confirmation
   */
  List<Integer> getConfirmationKeys(String protocolId);

  /**
   * Retrieves the confirmations for the provided voter ids
   *
   * @param protocolId the protocol instance identifier
   * @param voterIds   the voter indices
   *
   * @return for each provided voter id, the corresponding cast confirmation
   */
  Map<Integer, Confirmation> getConfirmations(String protocolId, List<Integer> voterIds);

  /**
   * Retrieve the result of the shuffle by control component at the given index
   *
   * @param protocolId the protocol instance identifier
   * @param ccIndex    the control component index
   *
   * @return the result of the shuffle by the control component at the provided index
   *
   * @throws IllegalStateException if the shuffle has not been set yet
   */
  List<Encryption> getShuffle(String protocolId, int ccIndex);

  /**
   * Retrieve the proof of validity of the shuffle performed by the control component at given index
   *
   * @param protocolId the protocol instance identifier
   * @param ccIndex    the control component index
   *
   * @return the proof of validity of the shuffle performed by control component at given index
   *
   * @throws IllegalStateException if the shuffle proof has not been set yet
   */
  ShuffleProof getShuffleProof(String protocolId, int ccIndex);

  /**
   * Retrieve the partial decryptions performed by the control component at the given index
   *
   * @param protocolId the protocol instance identifier
   * @param ccIndex    the control component index
   *
   * @return the partial decryptions performed by the control component at the given index
   *
   * @throws IllegalStateException if the decryptions have not been set yet
   */
  Decryptions getDecryptions(String protocolId, int ccIndex);

  /**
   * Retrieve the proof of validity of the partial decryption performed by the control component at the given index
   *
   * @param protocolId the protocol instance identifier
   * @param ccIndex    the control component index
   *
   * @return the proof of validity for the partial decryption performed by the control component at the given index
   *
   * @throws IllegalStateException if the proof has not been set yet
   */
  DecryptionProof getDecryptionProof(String protocolId, int ccIndex);

  /**
   * Clean up all the events generated by the bulletin board.
   *
   * @param protocolId the protocol instance Id.
   */
  void cleanUp(String protocolId);
}
