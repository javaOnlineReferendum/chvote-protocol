/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.message.api;

import ch.ge.ve.event.Event;
import ch.ge.ve.service.EventHeaders;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.support.converter.MessageConverter;

/**
 * A default implementation of a {@link MessageHandler} that implements a timeout for the
 * {@link #waitUntil(BooleanSupplier)} interface.
 *
 * @param <T> the message type.
 */
public abstract class MessageHandlerAbstractImpl<T extends Event> implements MessageHandler<T> {
  private final Class<T>                                         acceptedClass;
  private final String                                           protocolId;
  private final MessageConverter                                 converter;
  private final Map<BooleanSupplier, CompletableFuture<Boolean>> futureByPredicate = new HashMap<>();
  private final long                                             waitTimeout;


  private MessageListenerContainer container;
  private Consumer<T>              postProcessMessageEventConsumer;


  protected MessageHandlerAbstractImpl(long waitTimeout,
                                       Class<T> acceptedClass,
                                       String protocolId,
                                       MessageConverter converter) {
    this.waitTimeout = waitTimeout;
    this.acceptedClass = acceptedClass;
    this.protocolId = protocolId;
    this.converter = converter;
  }

  @Override
  public void stop() {
    container.stop();
    clear();
  }


  @Override
  public MessageListenerContainer getContainer() {
    return container;
  }

  @Override
  public void setContainer(MessageListenerContainer container) {
    this.container = container;
  }


  public void waitUntil(BooleanSupplier predicate) {
    if (!predicate.getAsBoolean()) {
      CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
      futureByPredicate.put(predicate, completableFuture);
      try {
        completableFuture.get(waitTimeout, TimeUnit.MILLISECONDS);
      } catch (ExecutionException | TimeoutException e) {
        throw new IllegalStateException("Predicate failed or timeout expired", e);
      } catch (InterruptedException e) {
        LoggerFactory.getLogger(getClass()).warn("Thread interrupted !", e);
        // Restore interrupted state...
        Thread.currentThread().interrupt();
      }
    }
    // if the predicate is true, we can return immediately
  }


  private void checkPredicates() {
    ImmutableMap.copyOf(futureByPredicate).forEach((predicate, completableFuture) -> {
      if (predicate != null && predicate.getAsBoolean()) {
        completableFuture.complete(true);
        futureByPredicate.remove(predicate);
      }
    });
  }

  @Override
  public void handleMessage(Message message) throws ClassNotFoundException {
    Map<String, Object> headers = message.getMessageProperties().getHeaders();
    if (!protocolId.equals(headers.get(EventHeaders.PROTOCOL_ID))) {
      return;
    }
    Class messageBodyClass = Class.forName(String.valueOf(headers.get("__TypeId__")));
    if (acceptedClass.isAssignableFrom(messageBodyClass)) {
      final T castMessage = acceptedClass.cast(converter.fromMessage(message));

      handleEvent(castMessage);
      checkPredicates();
      Optional.ofNullable(postProcessMessageEventConsumer).ifPresent(processor -> processor.accept(castMessage));
    }
  }

  @Override
  public void postProcessEvent(Consumer<T> messageEventConsumer) {
    this.postProcessMessageEventConsumer = messageEventConsumer;
  }

  protected abstract void handleEvent(T event);


  /**
   * Clear the state of this message handler.
   */
  protected abstract void clear();

}
