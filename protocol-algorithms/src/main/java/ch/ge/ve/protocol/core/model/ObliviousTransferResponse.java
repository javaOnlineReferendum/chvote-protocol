/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

/**
 * Model class for an Oblivious Transfer response
 */
public final class ObliviousTransferResponse {
  private final List<BigInteger> bold_b;
  private final CiphertextMatrix bold_upper_c;
  private final BigInteger       d;

  @JsonCreator
  public ObliviousTransferResponse(@JsonProperty("bold_b") List<BigInteger> bold_b,
                                   @JsonProperty("bold_upper_c") CiphertextMatrix bold_upper_c,
                                   @JsonProperty("d") BigInteger d) {
    Preconditions.checkNotNull(bold_upper_c, "c must not be null");
    this.bold_b = ImmutableList.copyOf(bold_b);
    this.bold_upper_c = new CiphertextMatrix(bold_upper_c.getValuesMap());
    this.d = d;
  }

  public List<BigInteger> getBold_b() {
    return bold_b;
  }

  public CiphertextMatrix getBold_upper_c() {
    return new CiphertextMatrix(bold_upper_c.getValuesMap());
  }

  public BigInteger getD() {
    return d;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ObliviousTransferResponse that = (ObliviousTransferResponse) o;
    return Objects.equals(bold_b, that.bold_b) &&
           Objects.equals(bold_upper_c, that.bold_upper_c) &&
           Objects.equals(d, that.d);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bold_b, bold_upper_c, d);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).add("b", bold_b).add("c", bold_upper_c).add("d", d)
                      .toString();
  }
}
