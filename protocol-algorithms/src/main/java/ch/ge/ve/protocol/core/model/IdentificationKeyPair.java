/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.IdentificationPublicKey;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model class representing an identification public/private key pair.
 */
public class IdentificationKeyPair extends KeyPair {

  @JsonCreator
  public IdentificationKeyPair(@JsonProperty("identificationPublicKey") IdentificationPublicKey identificationPublicKey,
                               @JsonProperty("identificationPrivateKey") IdentificationPrivateKey identificationPrivateKey) {
    super(identificationPublicKey, identificationPrivateKey);
  }

  @Override
  @JsonProperty("identificationPublicKey")
  public IdentificationPublicKey getPublicKey() {
    return (IdentificationPublicKey) super.getPublicKey();
  }

  @Override
  @JsonProperty("identificationPrivateKey")
  public IdentificationPrivateKey getPrivateKey() {
    return (IdentificationPrivateKey) super.getPrivateKey();
  }
}
