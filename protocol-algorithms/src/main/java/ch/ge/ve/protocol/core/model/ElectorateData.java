/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.Point;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.util.List;

/**
 * Model class holding data for the whole electorate
 */
public final class ElectorateData implements Serializable {
  private final List<SecretVoterData> d;
  private final List<Point>           d_hat;
  private final List<List<Point>>     upper_p;
  private final List<List<Integer>>   upper_k;

  @JsonCreator
  public ElectorateData(@JsonProperty("d") List<SecretVoterData> secretVoterDataList,
                        @JsonProperty("d_hat") List<Point> publicVoterDataList,
                        @JsonProperty("upper_p") List<List<Point>> randomPoints,
                        @JsonProperty("upper_k") List<List<Integer>> allowedSelections) {
    this.d = ImmutableList.copyOf(secretVoterDataList);
    this.d_hat = ImmutableList.copyOf(publicVoterDataList);
    this.upper_p = ImmutableList.copyOf(randomPoints);
    this.upper_k = ImmutableList.copyOf(allowedSelections);
  }

  public List<SecretVoterData> getD() {
    return d;
  }

  public List<Point> getD_hat() {
    return d_hat;
  }

  public List<List<Point>> getUpper_p() {
    return upper_p;
  }

  public List<List<Integer>> getUpper_k() {
    return upper_k;
  }
}
